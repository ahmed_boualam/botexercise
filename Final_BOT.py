#!/usr/bin/env python
# coding: utf-8

# ## Imports

# In[1]:


try:
    import Tkinter as tk
except:
    import tkinter as tk
    
import time
import hmac
from typing import Optional, Dict, Any, List
from requests import Request, Session, Response
from tkinter import font as tkFont
from tkinter import messagebox
import uuid

import hmac
import json
import time
import zlib
from collections import defaultdict, deque
from itertools import zip_longest
from typing import DefaultDict, Deque, List, Dict, Tuple, Optional
from gevent.event import Event

import json
import time
from threading import Thread, Lock

from websocket import WebSocketApp
import time
import sqlite3


# In[2]:



class WebsocketManager:
    _CONNECT_TIMEOUT_S = 15

    def __init__(self):
        self.connect_lock = Lock()
        self.ws = None

    def _get_url(self):
        raise NotImplementedError()

    def _on_message(self, ws, message):
        raise NotImplementedError()

    def send(self, message):
        self.connect()
        self.ws.send(message)

    def send_json(self, message):
        self.send(json.dumps(message))

    def _connect(self):
        assert not self.ws, "ws should be closed before attempting to connect"

        self.ws = WebSocketApp(
            self._get_url(),
            on_message=self._wrap_callback(self._on_message),
            on_close=self._wrap_callback(self._on_close),
            on_error=self._wrap_callback(self._on_error),
        )

        wst = Thread(target=self._run_websocket, args=(self.ws,))
        wst.daemon = True
        wst.start()

        # Wait for socket to connect
        ts = time.time()
        while self.ws and (not self.ws.sock or not self.ws.sock.connected):
            if time.time() - ts > self._CONNECT_TIMEOUT_S:
                self.ws = None
                return
            time.sleep(0.1)

    def _wrap_callback(self, f):
        def wrapped_f(ws, *args, **kwargs):
            if ws is self.ws:
                try:
                    f(ws, *args, **kwargs)
                except Exception as e:
                    raise Exception(f'Error running websocket callback: {e}')
        return wrapped_f

    def _run_websocket(self, ws):
        try:
            ws.run_forever()
        except Exception as e:
            raise Exception(f'Unexpected error while running websocket: {e}')
        finally:
            self._reconnect(ws)

    def _reconnect(self, ws):
        assert ws is not None, '_reconnect should only be called with an existing ws'
        if ws is self.ws:
            self.ws = None
            ws.close()
            self.connect()

    def connect(self):
        if self.ws:
            return
        with self.connect_lock:
            while not self.ws:
                self._connect()
                if self.ws:
                    return

    def _on_close(self, ws):
        self._reconnect(ws)

    def _on_error(self, ws, error):
        self._reconnect(ws)

    def reconnect(self) -> None:
        if self.ws is not None:
            self._reconnect(self.ws)


# In[3]:


class FtxWebsocketClient(WebsocketManager):
    _ENDPOINT = 'wss://ftx.com/ws/'

    def __init__(self) -> None:
        super().__init__()
        self._trades: DefaultDict[str, Deque] = defaultdict(lambda: deque([], maxlen=10000))
        self._fills: Deque = deque([], maxlen=10000)
        self._api_key = ''  # TODO: Place your API key here
        self._api_secret = ''  # TODO: Place your API secret here
        self._orderbook_update_events: DefaultDict[str, Event] = defaultdict(Event)
        self._reset_data()

    def _on_open(self, ws):
        self._reset_data()

    def _reset_data(self) -> None:
        self._subscriptions: List[Dict] = []
        self._orders: DefaultDict[int, Dict] = defaultdict(dict)
        self._tickers: DefaultDict[str, Dict] = defaultdict(dict)
        self._orderbook_timestamps: DefaultDict[str, float] = defaultdict(float)
        self._orderbook_update_events.clear()
        self._orderbooks: DefaultDict[str, Dict[str, DefaultDict[float, float]]] = defaultdict(
            lambda: {side: defaultdict(float) for side in {'bids', 'asks'}})
        self._orderbook_timestamps.clear()
        self._logged_in = False
        self._last_received_orderbook_data_at: float = 0.0

    def _reset_orderbook(self, market: str) -> None:
        if market in self._orderbooks:
            del self._orderbooks[market]
        if market in self._orderbook_timestamps:
            del self._orderbook_timestamps[market]

    def _get_url(self) -> str:
        return self._ENDPOINT

    def _login(self) -> None:
        ts = int(time.time() * 1000)
        self.send_json({'op': 'login', 'args': {
            'key': self._api_key,
            'sign': hmac.new(
                self._api_secret.encode(), f'{ts}websocket_login'.encode(), 'sha256').hexdigest(),
            'time': ts,
        }})
        self._logged_in = True

    def _subscribe(self, subscription: Dict) -> None:
        self.send_json({'op': 'subscribe', **subscription})
        self._subscriptions.append(subscription)

    def _unsubscribe(self, subscription: Dict) -> None:
        self.send_json({'op': 'unsubscribe', **subscription})
        while subscription in self._subscriptions:
            self._subscriptions.remove(subscription)

    def get_fills(self) -> List[Dict]:
        if not self._logged_in:
            self._login()
        subscription = {'channel': 'fills'}
        if subscription not in self._subscriptions:
            self._subscribe(subscription)
        return list(self._fills.copy())

    def get_orders(self) -> Dict[int, Dict]:
        if not self._logged_in:
            self._login()
        subscription = {'channel': 'orders'}
        if subscription not in self._subscriptions:
            self._subscribe(subscription)
        return dict(self._orders.copy())

    def get_trades(self, market: str) -> List[Dict]:
        subscription = {'channel': 'trades', 'market': market}
        if subscription not in self._subscriptions:
            self._subscribe(subscription)
        return list(self._trades[market].copy())

    def get_orderbook(self, market: str) -> Dict[str, List[Tuple[float, float]]]:
        subscription = {'channel': 'orderbook', 'market': market}
        if subscription not in self._subscriptions:
            self._subscribe(subscription)
        if self._orderbook_timestamps[market] == 0:
            self.wait_for_orderbook_update(market, 5)
        return {
            side: sorted(
                [(price, quantity) for price, quantity in list(self._orderbooks[market][side].items())
                 if quantity],
                key=lambda order: order[0] * (-1 if side == 'bids' else 1)
            )
            for side in {'bids', 'asks'}
        }

    def get_orderbook_timestamp(self, market: str) -> float:
        return self._orderbook_timestamps[market]

    def wait_for_orderbook_update(self, market: str, timeout: Optional[float]) -> None:
        subscription = {'channel': 'orderbook', 'market': market}
        if subscription not in self._subscriptions:
            self._subscribe(subscription)
        self._orderbook_update_events[market].wait(timeout)

    def get_ticker(self, market: str) -> Dict:
        subscription = {'channel': 'ticker', 'market': market}
        if subscription not in self._subscriptions:
            self._subscribe(subscription)
        return self._tickers[market]

    def _handle_orderbook_message(self, message: Dict) -> None:
        market = message['market']
        subscription = {'channel': 'orderbook', 'market': market}
        if subscription not in self._subscriptions:
            return
        data = message['data']
        if data['action'] == 'partial':
            self._reset_orderbook(market)
        for side in {'bids', 'asks'}:
            book = self._orderbooks[market][side]
            for price, size in data[side]:
                if size:
                    book[price] = size
                else:
                    del book[price]
            self._orderbook_timestamps[market] = data['time']
        checksum = data['checksum']
        orderbook = self.get_orderbook(market)
        checksum_data = [
            ':'.join([f'{float(order[0])}:{float(order[1])}' for order in (bid, offer) if order])
            for (bid, offer) in zip_longest(orderbook['bids'][:100], orderbook['asks'][:100])
        ]

        computed_result = int(zlib.crc32(':'.join(checksum_data).encode()))
        if computed_result != checksum:
            self._last_received_orderbook_data_at = 0
            self._reset_orderbook(market)
            self._unsubscribe({'market': market, 'channel': 'orderbook'})
            self._subscribe({'market': market, 'channel': 'orderbook'})
        else:
            self._orderbook_update_events[market].set()
            self._orderbook_update_events[market].clear()

    def _handle_trades_message(self, message: Dict) -> None:
        self._trades[message['market']].append(message['data'])

    def _handle_ticker_message(self, message: Dict) -> None:
        self._tickers[message['market']] = message['data']

    def _handle_fills_message(self, message: Dict) -> None:
        self._fills.append(message['data'])

    def _handle_orders_message(self, message: Dict) -> None:
        data = message['data']
        self._orders.update({data['id']: data})

    def _on_message(self, ws, raw_message: str) -> None:
        message = json.loads(raw_message)
        message_type = message['type']
        if message_type in {'subscribed', 'unsubscribed'}:
            return
        elif message_type == 'info':
            if message['code'] == 20001:
                return self.reconnect()
        elif message_type == 'error':
            raise Exception(message)
        channel = message['channel']

        if channel == 'orderbook':
            self._handle_orderbook_message(message)
        elif channel == 'trades':
            self._handle_trades_message(message)
        elif channel == 'ticker':
            self._handle_ticker_message(message)
        elif channel == 'fills':
            self._handle_fills_message(message)
        elif channel == 'orders':
            self._handle_orders_message(message)


# In[4]:


ws = FtxWebsocketClient()
ws.connect()


# In[5]:


print(ws.get_ticker(market='BTC/USD'))


# In[6]:


api_key = "TruLs-RrJsoDsMwMzJdel-C-p3dyE0DbQj1EQZuO"
api_secret = "_d4JJhUTm1L8IqK1gHUVo4xTHookDJ-fHqQeq0sb"


# In[7]:


class FtxClient:
    
    _ENDPOINT = 'https://ftx.com/api/'

    def __init__(self, api_key=api_key, api_secret=api_secret, subaccount_name=None) -> None:
        self._session = Session()
        self._api_key = api_key
        self._api_secret = api_secret
        self._subaccount_name = subaccount_name
        
    def _get(self, path: str, params: Optional[Dict[str, Any]] = None) -> Any:
        return self._request('GET', path, params=params)
    
    def _post(self, path: str, params: Optional[Dict[str, Any]] = None) -> Any:
        return self._request('POST', path)

    def _request(self, method: str, path: str, **kwargs) -> Any:
        request = Request(method, self._ENDPOINT + path)
        self._sign_request(request)
        response = self._session.send(request.prepare())
        return self._process_response(response)

    def _sign_request(self, request: Request) -> None:
        ts = int(time.time() * 1000)
        prepared = request.prepare()
        signature_payload = f'{ts}{prepared.method}{prepared.path_url}'.encode()
        if prepared.body:
            signature_payload += prepared.body
        signature = hmac.new(self._api_secret.encode(), signature_payload, 'sha256').hexdigest()
        request.headers['FTX-KEY'] = self._api_key
        request.headers['FTX-SIGN'] = signature
        request.headers['FTX-TS'] = str(ts)
        if self._subaccount_name:
            request.headers['FTX-SUBACCOUNT'] = urllib.parse.quote(self._subaccount_name)

    def _process_response(self, response: Response) -> Any:
        
        try:
            data = response.json()
        except ValueError:
            response.raise_for_status()
            raise
            
        else:
            if not data['success']:
                raise Exception(data['error'])
            return data['result']
        
    def place_order(self, market: str, side: str, price: float, size: float, client_id: str,
                    type: str = 'limit', reduce_only: bool = False, ioc: bool = False, post_only: bool = False,
                    ) -> dict:
        return self._post('orders', {'market': market,
                                     'side': side,
                                     'price': price,
                                     'size': size,
                                     'type': type,
                                     'reduceOnly': reduce_only,
                                     'ioc': ioc,
                                     'postOnly': post_only,
                                     'clientId': client_id,
                                     })
    
    def get_open_orders(self, order_id: int, market: str = None) -> List[dict]:
        return self._get(f'orders', {'market': market, 'order_id':order_id})


# In[8]:


class Gui():
    
    print(ws.get_ticker(market='BTC/USD'))

    def __init__(self,ftxclient):
        self.ftxclient = ftxclient
        self.root = tk.Tk()
        self.label = tk.Label(text="", font=('Helvetica', 18), fg='black')
        self.root.geometry('500x110')
        self.label.pack(side=tk.BOTTOM)
        self.userID = "0000"
        
        self.root.title('BTC/USD')
        helv36 = tkFont.Font(family='Helvetica', size=16)
        helv6 = tkFont.Font(family='Helvetica', size=11)


        self.button_sell = tk.Button(self.root,
                                 text="Sell",
                                command=self.sell_button_action,height = 5, 
          width = 20,font = helv36,fg='red',bg='black')

        self.button_buy = tk.Button(self.root,
                                text="Buy",
                                command=self.buy_button_action,height = 5, 
          width = 20,font = helv36,bg='black',fg='blue')
        
        self.previous_trades = tk.Button(self.root,
                                text="Trades",
                                command=self.trades,height = 3, 
          width = 7,font = helv6,bg='black',fg='blue')
        
        
        self.button_sell.pack(side=tk.LEFT)
        self.button_buy.pack(side=tk.RIGHT)
        self.previous_trades.pack(side=tk.BOTTOM)

        self.entry1 = tk.Entry(self.root) 
        
        self.entry1.insert(-1, '0.01')
        self.entry1.pack(side=tk.TOP) 
        
        self.update_clock()
        self.root.mainloop()
        
        
        
    def changeText_button_sell(self,texte,):
        
        a = ws.get_ticker(market='BTC/USD')
        stuff_in_string = f"Sell\n{texte} USD"
        self.button_sell['text'] = stuff_in_string
    
    def changeText_button_buy(self,texte):
        
        a = ws.get_ticker(market='BTC/USD')
        stuff_in_string = f"Buy\n{texte} USD"
        self.button_buy['text'] = stuff_in_string
        
    def sell_button_action(self):
        
        quantity = self.entry1.get()
        a = ws.get_ticker(market='BTC/USD')
        bidSize = a['bidSize']
        timestamp_trade = a['time']
        timenow = time.time()

        transactionID = str(uuid.uuid4())

        print(timestamp_trade)
        print(timenow)

        if (float(quantity) > float(bidSize)):
            messagebox.showwarning("error", "The quantity is higher than the max allowed.")
            
        elif (float(timenow)-float(timestamp_trade)) > 2:
            messagebox.showwarning("error", "Old Trade")
            
        else:
            try:
                self.ftxclient.place_order("ETH/USD", "sell", a['bid'], quantity, self.userID )
                
                payload = [transactionID,'sell',str(bidSize),str(quantity),str(timenow),'Success']
                payload = tuple(payload)
            
                ## Insert in the database
                database_value  = "INSERT INTO stocks VALUES " + str(payload)
                cur.execute(database_value)
                
                newWindow = tk.Toplevel(self.root)
 
                # sets the title of the
                # Toplevel widget
                newWindow.title("Transaction")
                # sets the geometry of toplevel
                newWindow.geometry("700x400")
                table_list = [('TradeID', 'Type', 'Selected Size', 'Price', 'Timestamp', 'Trade Status')]
                
                
                table_list.append(payload)
                
                total_rows = len(table_list)
                total_columns = len(table_list[0])

                for i in range(total_rows):
                    for j in range(total_columns):

                            self.e = tk.Entry(newWindow, width=15, fg='black',
                                               font=('Arial',10,'bold'))
                            self.e.grid(row=i, column=j)
                            self.e.insert(-1, table_list[i][j])
                    
            except Exception as e:
                messagebox.showwarning("error", e)
                print(e)
                
                
    def trades(self):
        
        newWindow = tk.Toplevel(self.root)
        # sets the title of the
        # Toplevel widget
        newWindow.title("Previous Trades")

        # sets the geometry of toplevel
        newWindow.geometry("700x400")

        # A Label widget to show in toplevel
        
        table_list = [('TradeID', 'Type', 'Selected Size', 'Price', 'Timestamp', 'Trade Status')]
        
        for row in cur.execute('SELECT * FROM stocks ORDER BY Timestamp'):
            table_list.append(row)
    
        print(table_list)
        
        total_rows = len(table_list)
        total_columns = len(table_list[0])
        
        for i in range(total_rows):
            for j in range(total_columns):

                    self.e = tk.Entry(newWindow, width=15, fg='black',
                                       font=('Arial',10,'bold'))

                    self.e.grid(row=i, column=j)
                    self.e.insert(-1, table_list[i][j])
                    

                            
    def buy_button_action(self):
        
        transactionID = str(uuid.uuid4())
        
        quantity = self.entry1.get()
        a = ws.get_ticker(market='BTC/USD')
        askSize = a['askSize']
        timestamp_trade = a['time']
        print(timestamp_trade)
        timenow = time.time()
        print(timenow)
        
        ## Case if the quantity is too high

        if float(quantity) > float(askSize):
            
            messagebox.showwarning("error", "The quantity is higher than the max allowed.")
            payload = [transactionID,'buy',str(bidSize),str(quantity),str(timenow),'Failed']
            payload = tuple(payload)
            print(payload)
            
            database_value  = "INSERT INTO stocks VALUES " + str(payload)
            cur.execute(database_value)

        ## Case if the trade is too old
        elif (float(timenow)-float(timestamp_trade))> 2:
            
            messagebox.showwarning("error", "Old Trade")
            payload = [transactionID,'buy',str(bidSize),str(quantity),str(timenow),'Failed']
            payload = tuple(payload)
            
            database_value  = "INSERT INTO stocks VALUES " + str(payload)
            cur.execute(database_value)
        
        
            

        else:
            
            try:
                self.ftxclient.place_order("ETH/USD", "buy", a['ask'], quantity, self.userID)
                payload = [transactionID,'buy',str(bidSize),str(quantity),str(timenow),'Success']
                payload = tuple(payload)
            
                ## Insert in the database
                database_value  = "INSERT INTO stocks VALUES " + str(payload)
                cur.execute(database_value)
                
                newWindow = tk.Toplevel(self.root)
 
                # sets the title of the
                # Toplevel widget
                newWindow.title("Transaction")
                # sets the geometry of toplevel
                newWindow.geometry("700x400")
                table_list = [('TradeID', 'Type', 'Selected Size', 'Price', 'Timestamp', 'Trade Status')]
                
                
                table_list.append(payload)
                
                total_rows = len(table_list)
                total_columns = len(table_list[0])

                for i in range(total_rows):
                    for j in range(total_columns):

                            self.e = tk.Entry(newWindow, width=15, fg='black',
                                               font=('Arial',10,'bold'))
                            self.e.grid(row=i, column=j)
                            self.e.insert(-1, table_list[i][j])
                    
            
            except Exception as e:
                messagebox.showwarning("error", e)
                print(e)
            
    

    def update_clock(self):
        
        now = time.strftime("%H:%M:%S")
        a = ws.get_ticker(market='BTC/USD')
        #text_now = a['bid']
        
        self.changeText_button_sell(a['bid'])
        self.changeText_button_buy(a['ask'])

        self.label.configure(text=now)
        self.root.after(100, self.update_clock)


# In[9]:


if __name__ == '__main__':
    con = sqlite3.connect('trades.db')
    cur = con.cursor()
    ftxclient = FtxClient()
    app = Gui(ftxclient)


# In[ ]:




